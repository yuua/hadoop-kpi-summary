## Hadoop本写経 2~3章

### mapreduce

#### 使い分け

##### map関数

-

map関数には処理対象の全体の中のデータ一つ一つが渡される。この時個々のmap関数にデータが渡される順序は制御できない。
よって、map関数は処理すデータ間で依存関係がなく独立に実行可能な処理や、順序を考慮しない処理を実装する


+ ex

> データのクレンジング処理や特定の条件にマッチしたデータを除外するフィルタリング、集約処理に先立っての前処理など

##### reduce関数

-

reduce関数にはキーに対してキーに紐付いた複数のデータが渡される。
またreduce関数に渡されるデータはキーでソートされている。よって、一度にあるまとまりをもった複数のデータを必要とする処理や順序を考慮する必要がある
処理を実装する。

+ ex

> いくつかの分類軸ごとの合計や平均値計算などの集約処理、時系列データを処理するう場合などに向いている

##### shuffle

map関数reduce関数の中間に当たる処理系統
shuffleフェーズでは同じキーをもつ中間データ同士がまとめられる。
実現したい処理が集約処理を伴う場合や、何らかの分類を行う場合、どのような軸で集約・分類を行うのかを考慮し
map関数で中間データに付与するキーを選択する

#### mapreduceジョブの分割

処理が複雑な場合複雑な処理をいくつかの単純な処理に分割すれば、複数のmapreduceジョブとして実現できる場合がある。
実現しようとしている処理が個々のデータの書こうと加工したデータの集約というパターンの組み合わせに分割できないか検討してみる

#### map関数だけで処理が観月できるか検討

mapreduceはHadoopの場合、mapフェーズとreduceフェーズのあいだのshuffleフェーズでは必ず中間データの受け渡しのための通信が発生する。
この通信のオーバーヘッドを回避するために実現しようとしている処理がmap関数だけでかんけつできないかどうかを検討するといい。
hadoopは処理の失敗のハンドリングなどを分散処理に関わる面倒な処理をフレームワーク側でみてくれるため、単純な並列処理でも適用するメリットがある。


#### hadoopの環境

##### 動作モード

3種類ある

+ ローカルモード

> 1台のサーバ上でHDFSは使用せずにmapreduceの動作環境を構築する

+ 擬似分散モード

> 1台のサーバ上でHDFSを使用したMapreduceの動作環境を構築する

+ 完全分散モード

> 複数台のサーバ上でHDFSを使用したMapReduceの動作環境を構築する

#### インストール

+ OS

linuxディストリビューションなどで動作する

+ jdk

oracle JDKの使用を推奨

+ CDH

Hadoopoの各種がつまったパッケージ

> 擬似分散モードの場合は hadoop-0.20-confpsudo が存在するのでテストとかでは使うと良さそう
使用するには Clouderaが提供するyumリポジトリの登録が必要

```
sudo rpm --import http://archive.cloudera.com/cdh4/redhat/6/x86_64/cdh/ RPM-GPG-KEY-cloudera
sudo rpm -ivh
rpm -ql cloudera-cdh
```

デフォルトでは最新バージョンを参照

```
sudo yum install hadoop-0.20-conf-pseudo
yum lista installed | grep hadoop # インストール物の確認
hadoo version
```

CDHのパッケージをインストールすると hdfsユーザとmapredユーザが作成される

> HDFSのサーバDaemonはhdfsユーザで実行され HDFS上のファイル操作ではhdfsユーザが特権ユーザになる
MapReduceのサーバDaemonはmapredユーザで実行される

+ NameNode のフォーマット

HDFSのprocessを立ち上げる前にNameNodeが管理するメタデータのフォーマットが必要

>hdfsユーザでの実行

```
sudo -u hdfs hdfs namenode -format
```

+ hdfs関連のプロセス起動

CDHはHDFS関連のprocessを起動するためのinitスクリプトを提供している

```
sudo service hadoop-hdfs-namenode start
```

+ mapreduce関連プロセスの起動

HDFSと同様にinitスクリプトを提供している

```
sudo service hadoop-0.20-mapreduce-jobtracker
sudo service hadoop-0.20-mapreduce-tasktracker
```
